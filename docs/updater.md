# GE:S Update Specification

## Introduction

The GoldenEye: Source installer, starting with v6.0, supports downloading updates over HTTP(S). These updates are automatically downloaded during installation. After the initial version of GE:S is installed, the update package is decompressed and the files inside it overwrite the existing files in the installation directory.

It is important to note this update system is only designed for trivial, bugfix-style updates: ones where the number of assets touched is minimal, and usually consists of things like binary or script changes. Larger updates, such as v5.0 to v6.0, will necessitate a full redownload.

## Update package format

The update package is a zip file which is downloaded by the installer. The installer then verifies the zip file using `minisign`, then decompresses it and copies its contents to the install folder. It also contains a file, `removed.txt`, which specifies which files to delete from the install folder.

There are two versions of update packages:

* Previous-to-latest, e.g. 5.1.4 -> 5.1.5
* Any-to-latest, e.g. 5.0.X -> 5.1.5

Both of these versions of patches are used by the patch system. If the user has the previous version installed, e.g. 5.1.4, the patch specifically for 5.1.4 is executed. If the user has an older version in the 5.1.X series, e.g. 5.1 or 5.1.1, the any-to-latest patch is used.

### Creating update packages

In the root of the zip will be the folder `gesource`, which is equivalent to the `gesource` directory under `sourcemods`. The patch root will also have a file named `removed.txt`, listing the files which are to be deleted.

```
gesource
└─ [patch files]
removed.txt
```

The `[patch root]/gesource` directory will contain all files that been added or changed from the previous version. In an any-to-latest patch, this includes all files that have been added or changed in ANY patch release since the initial version.

The `[patch root]/removed.txt` file will contain all files that were deleted since the previous version. In the any-to-latest patch, this must include ANY and ALL files that were deleted since the original version of GE:S. This includes any files that were added, and then removed in subsequent patches.

Each line will contain a file that will be deleted before beginning the archive unpacking process. Paths are relative to the `gesource` directory, and must be files -- not directories. They must not have a leading slash. For example:

```
maps/oldmap.bsp
materials/goldeneye/oldmap/texture.vtf
materials/goldeneye/oldmap/texture.vmt
```

## Update index data

The update index is a Valve KeyValues file. It has the top-level section `update`, under which the following key/value pairs are specified.

*Each minor version series must have its own update index file. Multiple version series in a single text file is not supported, by design. This is to avoid breaking old installers inadvertently.*

* `version`: the update version available

* `version_previous`: the version immediately preceding the latest version. If the current version is the initial release of this minor version series, set it to the same value as `version`.
  * If this matches the installed version of GE:S, the installer downloads the patch specified by `patch_previous`
  * If the installed version of GE:S is any other version, it downloads the patch specified by `patch_any`

* `download_page`: the URL that is opened when a user is prompted to download an updated installer.

* `changelog`: a link to the changelog

* `torrent`: a link to the current version's .torrent file, used by the network installer. Note that the network installer does not execute the patch, except to update an existing install. In order to get new installs up to the latest version, this .torrent file needs to be current. Must have an accompanying `.minisig` file.

* `download_size`: Size of `gesource.zip` in MiB

* `install_size`: Size of the uncompressed `gesource` folder, in MiB

* `patch_previous`: a link to the patch .zip file for updating the previous version. Must have an accompanying `.minisig` file.

* `patch_any`: a link to the patch .zip file for updating older versions in the same release series. Must have an accompanying `.minisig` file.

* `installer_version`: the minimum installer version needed to install the latest update. If this is greater than the installer version currently running, the user is prompted to update. Network installers require the latest update if they're older than this. Full installers don't, but if the user declines to update to the latest installer, the installer will not check for or download updates. \
 \
 **Note**: Set this to `9999.99.99.9999` if a superceding version is released (e.g. 5.1 -> 6.0). This will prompt users to download the latest installer. Standalone installers will continue to work if bypassing the message.

* `patch_installer`: a link to the patch installer, to be downloaded via the updater. Must have an accompanying `.minisig` file.

* `updater_version`: minimum version of the [updater](https://gitlab.com/soupcan/ges-updater) necessary for this update. Similar to `installer_version`, if this is greater than the updater version, the updater will instead prompt the user to go to downloads webpage to download the update. In addition, if the installed updater's version is less than this `updater_version`, the installer will download a new updater to replace the installed version.

The following is a valid example of an update index file.

```
// Update index file for version 5.1.x

update
{

  version                   "5.1.5"

  version_previous          "5.1.4"

  download_page             "https://www.geshl2.com/download/"

  changelog                 "https://wiki.geshl2.com/v5.1.5_changelist"

  torrent                   "https://example.com/GoldenEye_Source_v5.1.5_full.torrent"

  download_size             "2000"

  install_size              "5580"

  patch_previous            "https://example.com/GoldenEye_Source_v5.1.4_to_v5.1.5_patch.zip"

  patch_any                 "https://example.com/GoldenEye_Source_v5.1.X_to_v5.1.5_patch.zip"

  installer_version         "2023.02.19.0000"

  patch_installer           "https://example.com/GoldenEye_Source_v5.1.6_netpatch.exe"

  updater_version           "2023.02.19.0000"

}
```