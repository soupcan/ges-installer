; common directory picker and validation logic for installer and uninstaller
; The starting directory is $INSTDIR, and it sets $INSTDIR to the selected
; directory.

!macro changeInstDir funcName
  Push $0
  Push $1
  Push $2
  Push $R0

  nsDialogs::SelectFolderDialog $(INST_BROWSE_TITLE) $INSTDIR
  Pop $0
  StrCmp $0 "error" Exit +1 ; Skip if error

    ; Source engine requirement, as well as precaution against the uninstaller
    ; wiping out unintended data
    ; Require install folder to be named ${MODDIR}
    ;
    ; Get last folder and its leading slash
    StrLen $1 "${MODDIR}"
    IntOp $1 $1 + 1
    StrCpy $2 $0 "" "-$1"
    ; $2 should now = "\${MODDIR}"
    StrCmp "\${MODDIR}" $2 Success +1
    ; Is not equal

    ; Prepare fixed string for messagebox + StrCpy
    ; Check if path already ends with a backslash
    ; This happens when the user chooses a drive root
    StrCpy $R0 "$0" 1024 -1 ; Get last char
    StrCmp $R0 "\" NoBackslash Backslash ; Is last char a backslash?
    Backslash:
      StrCpy $R0 "$0\${MODDIR}"
      Goto BackslashFixEnd
    NoBackslash:
      StrCpy $R0 "$0${MODDIR}"
      Goto BackslashFixEnd
    BackslashFixEnd:

    MessageBox MB_YESNOCANCEL|MB_ICONQUESTION "$(INST_DIR_FIX)" IDYES FIX_DIR IDNO RETRY
      Goto Exit ; if user selected ignore, cancel the directory change

      RETRY:
      Call ${funcName}
      Goto Exit

      FIX_DIR:
      StrCpy "$0" "$R0"

    ; Is equal
    Success:
    StrCpy $INSTDIR $0

  Exit:
  Pop $R0
  Pop $2
  Pop $1
  Pop $0
!macroEnd

Function changeInstDir
  !insertmacro changeInstDir changeInstDir
FunctionEnd

Function un.changeInstDir
  !insertmacro changeInstDir un.changeInstDir
FunctionEnd
