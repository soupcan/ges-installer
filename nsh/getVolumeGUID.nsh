; This function gets the unique volume path of the volume hosting the provided provided path and returns it to the stack.
; Useful for comparing if two folders reside on the same volume, e.g. for dealing with NTFS mount points

; Usage:
; Push drive_letter | path
; Call getVolumeGUID
; Pop $OutVar

Function getVolumeGUID
  Exch $0

  nsExec::ExecToStack "powershell.exe -c echo (Get-Volume -FilePath '$0').Path"
  Pop $0 ; exit code
  Pop $0 ; This should be our volume path

  Exch $0

FunctionEnd
