; by soupcan for the GoldenEye: Source Installer
; This function accepts an input version string (e.g. x.x.x)
; and returns the minor version (e.g. x.x)
; Usage:
; Push $var
; Call (un.) GetMinorVersion
; Pop $var

!ifdef DEBUG
  !define _GMV_MESSAGEBOX "MessageBox"
!else
  !define _GMV_MESSAGEBOX ";"
!endif

!macro _GMV_MainMacro
  Exch $9 ; input
  Push $R9 ; output
  Push $0
  Push $1
  Push $R0
  Push $R1

  ; Function begin
  StrCpy $R0 '' ; working var
  StrCpy $R1 '' ; input string length
  StrCpy $0 0 ; loop accumulator
  StrCpy $1 0 ; period accumulator
  ; $9 = input
  ; $R9 = output

  StrLen $R1 $9
  StrCmp $R1 0 end ; Goto end if strlen == 0

  loop:
    IntCmp $0 $R1 +1 +1 out_of_chars ; finish if accumulator greater than strlen
    StrCpy $R0 $9 1 $0 ; get character number $0 of input
    ${_GMV_MESSAGEBOX} MB_OK "Character $0 is $R0"
    IntOp $0 $0 + 1 ; add 1 to the accumulator
    StrCmp $R0 "." equal +1 ; if current character is a period, goto equal
      Goto loop ; otherwise, goto loop
    out_of_chars:
      StrCpy $1 2
    equal:
      IntOp $1 $1 + 1 ; add 1 to the period accumulator
      IntCmp $1 2 +1 loop +1 ; if number of periods >= 2, continue, else goto loop
      IntOp $0 $0 - 1 ; subtract 1 from accumulator so as to not collect the last period
      StrCpy $R9 $9 $0 ; get string up to $0 chars
  end:
  ${_GMV_MESSAGEBOX} MB_OK "Macro returned with the string '$R9'$\r$\n$$0 = '$0'$\r$\n$$1 = '$1'$\r$\n$$R0 = '$R0'"

  Pop $R1
  Pop $R0
  Pop $1
  Pop $0
  Exch
  Pop $9
  Exch $R9
!macroend

Function GetMinorVersion
  !insertmacro _GMV_MainMacro
FunctionEnd

Function un.GetMinorVersion
  !insertmacro _GMV_MainMacro
FunctionEnd

Section "-Shut up NSIS"
Return
Call GetMinorVersion
SectionEnd

Section "-un.Shut up NSIS"
Return
Call un.GetMinorVersion
SectionEnd
