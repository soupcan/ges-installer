; compression settings
SetCompress auto
SetCompressor /SOLID lzma
SetDatablockOptimize ON
; Generate ${INSTALLER_BUILD_TIME} for file version, if not already defined
!ifndef INSTALLER_BUILD_TIME
  !tempfile __BUILTON
  !system 'powershell.exe -c "Write-Host -NoNewLine $\'!define INSTALLER_BUILD_TIME $\'; \
          (Get-Date).ToUniversalTime().ToString($\'yyyy.M.d.Hmm$\')" > "${__BUILTON}"'
  !include "${__BUILTON}"
  !delfile "${__BUILTON}"
!endif

; Multi-language support
Unicode true
; NSIS scales its windows instead of the OS upscaling it. Reduces blur
ManifestDPIAware true

!addplugindir "bin"

; Hide warning for when optional "patch" folder is missing
!pragma warning disable 7010

; Product/Version info
!include "version.nsh"

; NSIS header files
!include "MUI2.nsh"
!include "FileFunc.nsh"
!include "WordFunc.nsh"
!include "WinVer.nsh"
!include "x64.nsh"
; non-NSIS header files
!include "nsh\3rd-party\Trim.nsh"
!include "nsh\3rd-party\DumpLog.nsh"
!include "nsh\Symlink.nsh"
!include "nsh\GetMinorVersion.nsh"
!include "nsh\getDiskFree.nsh"
!include "nsh\changeInstDir.nsh"
!include "nsh\vdfParser.nsh"
!include "nsh\verifySignature.nsh"
!include "nsh\3rd-party\Validate.nsh"
!define HEXADECIMAL "0123456789ABCDEF"

!ifdef NETWORK
  !include "nsh\3rd-party\StrRep.nsh"
  !include "nsh\transmission.nsh"
  !include "nsh\getVolumeGUID.nsh"
!endif
; MUI2
;; Graphical resources
!define PBS_MARQUEE 0x08 ; for our progress bar on instfiles / unpage_instfiles
!define MUI_UI "bin\modern.exe" ;Value
!define MUI_ICON ".\resources\icon.ico" ; File icon
!define MUI_HEADERIMAGE_BITMAP ".\resources\headerimage.bmp" ; Page header image
!define MUI_HEADERIMAGE_RIGHT
;; Custom abort dialog/function
!ifndef PATCH
  !define MUI_CUSTOMFUNCTION_ABORT onUserAbort
!endif
; END MUI2

Name "${PROD_NAME}"

!ifdef FULL
  !define OUTFILE "${EXE_NAME_FULL}"
!else
  !ifdef PATCH
    !define OUTFILE "${EXE_NAME_PATCH}"
  !else
    !define OUTFILE "${EXE_NAME}"
  !endif
!endif
OutFile "${OUTFILE}"

; Variables
Var PLUGINSDIR_LONG ; Long path version of $PLUGINSDIR
Var STEAMEXE ; Path to Steam.exe
Var SOURCEMODS ; Sourcemods folder absolute path
Var GES_INSTALLED_VERSION ; Empty if GE:S is not installed.
                          ; Version number if it is.
Var GES_UPDATEABLE ; Is GE:S updateable using this installer?
                   ; 1 if yes, empty otherwise.
Var DISK_REQUIRED ; Number of MBs needed for successful install.
                  ; This number is checked before allowing the user to install.
Var INSTALL_BEGAN ; 1 if install has began (first install section executed)
Var INSTALL_OR_UPDATE ; Set to INSTALL or UPDATE based on the option being executed.
; Updater/downloader
Var SIGNATURE_CHECKING_ENABLED ; 0 disables signature checking; any other value enables
Var GES_UPDATE_CHECK_COMPLETED ; 1 if already successfully checked for updates
Var GES_UPDATE_INDEX ; Link to the update index
Var GES_UPDATE_VERSION ; Numeric version of the update available, e.g. 5.1.1
Var GES_UPDATE_CHANGELOG ; Link to wiki changelog for most recent version
Var GES_TORRENT ; URL to download .torrent file for network new install
Var GES_UPDATE_URL ; Link to download the .zip
Var GES_DOWNLOAD_SIZE ; Size of the GE:S download in MiB, set by update index
Var GES_INSTALL_SIZE ; Size of the uncompressed GE:S install
!ifdef PATCH
  Var AUTO_UPDATE ; 1 if Auto Update is enabled. This skips the intro/options
                  ; screens, and proceeds with the update.
!endif

; Welcome/License page
;; Are we skipping the License page?
Var SkipLicensePage

; INSTFILES custom prog bar
Var prgInstfiles ; main progress bar
Var prgInstfilesCustom ; our custom progress bar

!ifndef PATCH
  ; Install options page
  ;; Is this the first run of the page?
  Var InstallOptionsFirstRun
  ;; Remember the last couple of disk GUIDs to avoid the expensive getVolumeGUID
  !ifndef FULL
  Var UDR_PluginsdirGUID
  Var UDR_InstdirPath
  Var UDR_InstdirGUID
  !endif
  ;; Controls
  Var radUpdate
  Var radReinstall
  Var chkKeepAchievements
  Var lblDiskFreeAmount
  Var lblDiskRequiredAmount
  Var txtInstDir
  Var btnInstDir
  Var lnkResetInstDir
  Var chkCreateShortcut
  !ifdef NETWORK
    Var chkSeedTorrent
    Var lnkSeedTorrent
  !endif
  ;; Control states
  Var radUpdate_Checked
  Var radReinstall_Checked
  Var chkKeepAchievements_Checked
  Var chkCreateShortcut_Checked
  !ifdef NETWORK
    Var chkSeedTorrent_Checked
  !endif
  ; Install sections
  Var WAITING_ON_ABORT ; 1 if Abort dialog open, 0 if not
  !ifndef FULL
    Var DOWNLOAD_FOLDER ; GE:S download temp folder
    Var DAEMON_PID ; PID of our Transmission daemon instance
    ; (a bunch of other transmission vars are defined in transmission.nsh)
  !endif
!endif

; Pages
!define MUI_CUSTOMFUNCTION_GUIINIT custom.onGuiInit
!define MUI_WELCOMEPAGE_TITLE "$(WELCOME_TITLE)"
!define MUI_WELCOMEPAGE_TEXT "$(WELCOME_TEXT)"
!define MUI_WELCOMEFINISHPAGE_BITMAP "resources\banner.bmp"
!define MUI_PAGE_CUSTOMFUNCTION_PRE WelcomePagePre
!define MUI_PAGE_CUSTOMFUNCTION_SHOW WelcomePageShow
!define MUI_PAGE_CUSTOMFUNCTION_LEAVE WelcomePageLeave
!insertmacro MUI_PAGE_WELCOME
!define MUI_PAGE_CUSTOMFUNCTION_PRE LicensePre
!insertmacro MUI_PAGE_LICENSE "license.txt"
Page Custom UpdatePage
!ifndef PATCH
  Page Custom InstallOptsPage InstallOptsLeave
!endif
!insertmacro MUI_PAGE_INSTFILES
!define MUI_FINISHPAGE_TEXT $(FINISH_TEXT)
!define MUI_FINISHPAGE_LINK $(OPEN_CHANGELOG)
!define MUI_FINISHPAGE_LINK_LOCATION $GES_UPDATE_CHANGELOG
!define MUI_FINISHPAGE_NOREBOOTSUPPORT
!insertmacro MUI_PAGE_FINISH

; Uninstaller code
!include "nsh\uninstaller.nsh"

; Set VIProductVersion -- prerequisite to VIAddVersionKey in language files
VIProductVersion "${INSTALLER_BUILD_TIME}"
; Load language files
!include "nsh\lang_load.nsh"

; Status macro
!macro StatusSet Text
  SetDetailsPrint both
  DetailPrint "${Text}"
  SetDetailsPrint listonly
!macroend

!define Status "!insertmacro StatusSet"
; Status macro end

; Abort macro, aborts and shows log
!define Failure "!insertmacro Abort"

!macro Abort
  SetDetailsView show
  Call SaveDetailsView
  ${Status} $(INSTALL_FAILED)
  Abort
!macroend
; Abort macro end

; Macro you can place somewhere you need to hold the installer if the abort
; dialog is open
!macro WAIT_ON_ABORT
  !define WaitLoop "WaitLoop_${__LINE__}"
  !define NotWaiting "NotWaiting_${__LINE__}"

  ; if waiting on the abort dialog, show this status message.
  ; otherwise skip the status message and the loop, and jump straight to the
  ; end of the macro
  StrCmp $WAITING_ON_ABORT 1 +1 ${NotWaiting}
    ${Status} $(WAITING_ON_ABORT)

  ${WaitLoop}:
  StrCmp $WAITING_ON_ABORT 1 +1 ${NotWaiting}
    ; If waiting
    Sleep 100
    Goto ${WaitLoop}
  ${NotWaiting}:

  !undef WaitLoop
  !undef NotWaiting
!macroend

; Macro for downloading files
!macro download url dest outvar options
  DetailPrint 'Downloading ${url} -> "${dest}"'
  NSxfer::Transfer ${options} /URL "${url}" /LOCAL "${dest}" /REFERER "${HTTP_REFERER}" /END
  Pop ${outvar}
  DetailPrint "HTTP request returned ${outvar}"
!macroend

!macro unzip zipfile dest
  DetailPrint 'Extracting "${zipfile}" to "${dest}"'
  nsExec::Exec "powershell.exe -c $\"\
                Add-Type -Assembly 'System.IO.Compression.Filesystem';\
                [System.IO.Compression.ZipFile]::ExtractToDirectory('${zipfile}','${dest}')\
                $\""
  ; Delete exit code off the stack
  Exch $0
  Pop $0
!macroend

; Load install sections
!include "nsh\install_sections.nsh"

!ifndef PATCH
  Function onUserAbort
    StrCpy $WAITING_ON_ABORT 1
    MessageBox MB_YESNO $(ABORT) IDYES YES
      StrCpy $WAITING_ON_ABORT 0
      Abort ; abort the abortion

    YES:
    !ifndef PATCH
      ; Restore data if user aborts during install
      StrCmp $INSTALL_BEGAN 1 +1 DontRestoreData
        Call RestoreData

      DontRestoreData:
    !endif

    ; Letting this function return will abort the installation
  FunctionEnd
!endif

Function SaveDetailsView
  Push $0
  Push $1
  Push $2
  Push $3
  Push $4
  Push $5
  Push $6

  ; Get log file timestamp
  ${GetTime} "" "L" $0 $1 $2 $3 $4 $5 $6
  ; $0="01"      day
  ; $1="04"      month
  ; $2="2005"    year
  ; $4="16"      hour
  ; $5="05"      minute
  ; $6="50"      seconds
  StrCpy $0 "$TEMP\${MODDIR}-install-$2$1$0$4$5$6.log"

  SetDetailsPrint textonly
  DetailPrint "Writing log file..."
  SetDetailsPrint both
  Push "$0"
  Call DumpLog
  DetailPrint "Log written to $0"

  Pop $6
  Pop $5
  Pop $4
  Pop $3
  Pop $2
  Pop $1
  Pop $0
FunctionEnd

Function displayCmdHelp
  !ifdef FULL
    !define InstallerType "full installer (Updates will still be downloaded)"
  !else
    !ifdef PATCH
      !define InstallerType "patch installer"
    !else
      !define InstallerType "network installer"
    !endif
  !endif

  MessageBox MB_OK "\
    ${PROD_NAME} ${PROD_VERSION} ${InstallerType} $\r$\n\
    Build ${INSTALLER_BUILD_TIME} $\r$\n$\r$\n\
    \
    Command-line options: $\r$\n$\r$\n\
    \
    /?$\r$\n\
    Display this dialog$\r$\n\
    $\r$\n\
    /AutoUpdate$\r$\n\
    Patch installer only: Skip intro dialog and install options. Proceed to install update only. \
    This option is used when launched by the automatic updater.$\r$\n\
    $\r$\n\
    /DisableSignatureCheck$\r$\n\
    For testing purposes only: Disables signature checking of update index and downloaded files.$\r$\n\
    $\r$\n\
    /UpdateIndex=<url>$\r$\n\
    For testing purposes only: Specify a custom update.txt\
  "

  !undef InstallerType
FunctionEnd

!ifdef PATCH
  Function enableAutoUpdate
    StrCpy $AUTO_UPDATE 1
  FunctionEnd

  Function autoUpdate_PreInstallChecks
    ; If the installed and latest GE:S versions are the same,
    ; notify the user and exit.
    Push $0
    ${VersionCompare} $GES_INSTALLED_VERSION $GES_UPDATE_VERSION $0
    StrCmp $0 0 0 VersionsNotEquivalent
      MessageBox MB_OK|MB_ICONINFORMATION "$(PATCH_INSTALLER_UP_TO_DATE)"
      Quit
    VersionsNotEquivalent:
    Pop $0

    ; If GE:S is otherwise not updateable, display a generic error and exit
    StrCmp $GES_UPDATEABLE 1 Done
      MessageBox MB_OK|MB_ICONEXCLAMATION "$(PATCH_INSTALLER_UPDATE_NOT_APPLICABLE)"
      Quit
    Done:
  FunctionEnd
!endif

Function parseCmdOptions
  ; Default variable settings; may be overwritten by this section
  StrCpy $GES_UPDATE_INDEX "${UPDATE_FILE_URL}"
  StrCpy $SIGNATURE_CHECKING_ENABLED 1

  ${GetParameters} $0

  ; Search for any of a few "help" or "version" flags, and if present,
  ; display the help/about dialog
  Goto help
  help:
    ClearErrors
    ${GetOptions} "$0" "/?" "$1"
    IfErrors +1 displayHelp

    ClearErrors
    ${GetOptions} "$0" "-h" "$1"
    IfErrors +1 displayHelp

    ClearErrors
    ${GetOptions} "$0" "--help" "$1"
    IfErrors +1 displayHelp

    ClearErrors
    ${GetOptions} "$0" "--version" "$1"
    IfErrors +1 displayHelp

    ClearErrors
    ${GetOptions} "$0" "-v" "$1"
    IfErrors +1 displayHelp

    Goto SignatureChecking

    displayHelp:
      Call displayCmdHelp
      Quit

  SignatureChecking:
    ClearErrors
    ${GetOptions} "$0" "/DisableSignatureCheck" "$1"
    IfErrors +1 DisableSignatureChecking

    Goto UpdateIndex

    DisableSignatureChecking:
      StrCpy $SIGNATURE_CHECKING_ENABLED 0

  UpdateIndex:
    ClearErrors
    ${GetOptions} "$0" "/UpdateIndex=" "$1"
    IfErrors +1 CustomUpdateIndex

    Goto AutoUpdate

    CustomUpdateIndex:
      StrCpy $GES_UPDATE_INDEX $1

  AutoUpdate:
    ClearErrors
    ${GetOptions} "$0" "/AutoUpdate" "$1"
    IfErrors +1 enableAutoUpdate

    Goto Done

    enableAutoUpdate:
      !ifdef PATCH
        Call enableAutoUpdate
      !endif

  Done:

FunctionEnd

; Resets all update version info to that which came with the installer
Function clearUpdateInfo
  DetailPrint "Clearing update info"
  StrCpy $GES_UPDATE_VERSION '${PROD_VERSION}'
  StrCpy $GES_UPDATE_CHANGELOG '${CHANGELOG}'
  StrCpy $GES_UPDATE_URL ''
FunctionEnd

Function .onInit

  ; Check if running a supported operating system
  ${IfNot} ${AtLeastWin10}
  ${OrIfNot} ${RunningX64}
    MessageBox MB_OK "$(UNSUPT_OS)"
    Quit
  ${EndIf}

  ; Disable silent installs, which are untested and unsupported
  SetSilent normal

  Call parseCmdOptions

  ; Check if game running. If it is, prompt user on whether they want to continue.
  ; 0 = no process found, any other value = the PID of the running process.
  nsExec::Exec 'powershell.exe -c "exit (Get-CimInstance -ClassName Win32_Process -Filter \"Name = $\'hl2.exe$\' AND CommandLine LIKE $\'%${MODDIR}%$\'\").ProcessID"'
  Pop $0

  StrCmp $0 0 GameNotRunning
    MessageBox MB_OKCANCEL|MB_ICONQUESTION $(GAME_RUNNING) IDOK KillProcessAndContinue
      MessageBox MB_OK $(GAME_RUNNING_DECLINE)
      Quit

    KillProcessAndContinue:
      nsExec::Exec 'TASKKILL /F /PID $0'

  GameNotRunning:

  InitPluginsDir

  ; Get "long path" of $PLUGINSDIR, this is necessary for the windows firewall
  ; exception because windows firewall doesn't understand 8dot3 paths
  StrCpy $0 $PLUGINSDIR
  System::Call 'kernel32::GetLongPathName(t r0, t .r1, i ${NSIS_MAX_STRLEN}) i .r2'
  StrCpy $PLUGINSDIR_LONG $1

  ; By default only print log messages to the details view
  ; the text on the title bar will by updated by calling ${Status} [Text]
  SetDetailsPrint listonly

  ; Check for GUID of the drive containing $PLUGINSDIR
  ; and save it to var for later
  !ifndef FULL
    !ifndef PATCH
      Push $PLUGINSDIR
      Call getVolumeGUID
      Pop $R0
      StrCpy $UDR_PluginsdirGUID $R0 ; remember this GUID for later
    !endif
  !endif

  ; Get the Steam path from HKLM
  ;
  ; Why don't we use the user's HKCU to get the SourceModInstallPath?
  ;
  ; Because those keys don't exist until THAT SPECIFIC USER has run Steam.
  ;
  ; This means we can't get the Steam path in cases where the elevated account
  ; has never run Steam.
  ;
  ; This commonly occurs on multi-user systems, or systems where the user has a
  ; standard account for day-to-day use and an elevated account for software
  ; installations.

  ; Autoredirection for 32-bit programs will take care of Wow6432Node for us
  ReadRegStr $0 HKLM32 "Software\Valve\Steam" "InstallPath"

  ; Verify string is not empty
  StrCmp $0 "" Steam_Error +1

  ; Verify location exists
  IfFileExists "$0\*.*" +1 Steam_Error

  ; Set variables, clean up, finish Steam section
  StrCpy "$SOURCEMODS" "$0\steamapps\sourcemods"
  StrCpy "$STEAMEXE" "$0\Steam.exe"
  StrCpy $0 ""
  Goto Steam_Done

  Steam_Error:
    MessageBox MB_OK|MB_ICONEXCLAMATION $(STEAMERR)
    ExecShell open "https://store.steampowered.com/about/"
    Quit
  Steam_Done:

  StrCpy $INSTDIR "$SOURCEMODS\${MODDIR}"

  ; Always set this to UPDATE for the patch installer
  ; The patch installer doesn't include the install options page,
  ; so this variable otherwise isn't set in the patch installer.
  !ifdef PATCH
    StrCpy $INSTALL_OR_UPDATE 'UPDATE'
  !endif

FunctionEnd

Function custom.onGuiInit
  ; Get HWND
  GetDlgItem $0 $HWNDPARENT 4
  ; Set text
  SendMessage $0 ${WM_SETTEXT} 0 "STR:$(LICENSE)"
  ; Hide window; it will be reshown by pages as needed
  ShowWindow $0 0

  ; Invoke function upon pressing our License button
  GetFunctionAddress $0 LicenseButton
  ButtonEvent::AddEventHandler 4 $0
FunctionEnd

; Function run on GUI End, can be used e.g. to clean up temp files
Function .onGUIEnd
  ; stuff for the network installer only
  !ifndef FULL
  !ifndef PATCH
      ; Gracefully stop Transmission daemon, if it happens to be running
      IfFileExists $DAEMON_PID +1 nopid
        nsExec::ExecToLog 'taskkill /PID $DAEMON_PID'
        Pop $0

        Sleep 300
      nopid:

      ; Kill any transmission-remote & daemon instances forcibly as a fallback
      nsExec::ExecToLog 'taskkill /F /IM transmission-remote-ges.exe'
      nsExec::ExecToLog 'taskkill /F /IM transmission-daemon-ges.exe'
      Pop $0

      ; Remove temporary firewall exceptions
      Call DeleteTempFirewallException
    !endif
  !endif

  RMDir /r "$PLUGINSDIR"
FunctionEnd

Function WelcomePagePre
  !ifdef PATCH
    ; Skip this page if $AUTO_UPDATE is 1
    StrCmp $AUTO_UPDATE 1 +1 AbortAbort
      Abort
    AbortAbort:
  !endif
FunctionEnd

Function WelcomePageShow
  ; Skip License page by default; overwritten by License button
  StrCpy $SkipLicensePage 1
  ; Show license button
  GetDlgItem $0 $HWNDPARENT 4
  ShowWindow $0 1
  ; Enable Next button
  GetDlgItem $0 $HWNDPARENT 1
  ShowWindow $0 1
FunctionEnd

Function WelcomePageLeave
  ; Hide the License button
  GetDlgItem $0 $HWNDPARENT 4
  ShowWindow $0 0
FunctionEnd

Function LicenseButton
  ; Indicate to the License page not to skip it
  StrCpy $SkipLicensePage 0
  ; Advance the page by 1; go to the license page
  SendMessage $HWNDPARENT "0x408" "1" ""
FunctionEnd

Function LicensePre
  !ifdef PATCH
    ; Skip this page if $AUTO_UPDATE is 1
    StrCmp $AUTO_UPDATE 1 +1 AbortAbort
      Abort
    AbortAbort:
  !endif

  ; Skip the License page if indicated
  StrCmp $SkipLicensePage 1 +1 SkipSkip
    Abort ; Skip the License page
  SkipSkip:

  ; Disable Next button
  GetDlgItem $0 $HWNDPARENT 1
  ShowWindow $0 0
FunctionEnd

; This isn't a page, so much as it is a normal function.
; Abort is called to prevent the page from actually showing once the function completes.
Function UpdatePage
  !define PATCH_INDEX "$PLUGINSDIR\update.txt"

  !insertmacro MUI_HEADER_TEXT $(UPDATECHECK_H1) ""

  ; Don't check for updates if the update check has already completed
  StrCmp $GES_UPDATE_CHECK_COMPLETED 1 0 UpdateCheckBegin
    Abort

  UpdateCheckBegin:

  ; Check currently installed version
  ReadRegStr $0 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" "InstallLocation"
  ${Trim} $0 $0
  ; Don't set INSTDIR if key empty or path doesnt exist
  StrCmp $0 "" SetInstDirToDefault +1 ; if string empty, set to default
  IfFileExists "$0\*.*" +1 SetInstDirToDefault ; if doesnt exist, set to default
    StrCpy $INSTDIR $0
    Goto SetInstDirDone
  SetInstDirToDefault:
    StrCpy $INSTDIR "$SOURCEMODS\${MODDIR}"
    Goto SetInstDirDone
  SetInstDirDone:

  ClearErrors

  ; Get version number from version.txt
  IfFileExists "$INSTDIR\version.txt" 0 VersionCheckFinish
    ${ReadVDFStr} $0 "$INSTDIR\version.txt" "ges_version>major"
    ${ReadVDFStr} $1 "$INSTDIR\version.txt" "ges_version>minor"
    ${ReadVDFStr} $2 "$INSTDIR\version.txt" "ges_version>client"

    IfErrors VersionCheckError

    StrCpy $GES_INSTALLED_VERSION "$0.$1.$2"

    Goto VersionCheckFinish

    VersionCheckError:
      StrCpy $GES_INSTALLED_VERSION "0.0.0"

  VersionCheckFinish:

  ClearErrors

  ; Check for updates
  ; Get update index file
  !insertmacro download "$GES_UPDATE_INDEX" "${PATCH_INDEX}" $0 "/MODE SILENT"

  ; Get update index signature
  !insertmacro download "$GES_UPDATE_INDEX.minisig" "${PATCH_INDEX}.minisig" $0 "/MODE SILENT"

  ; Validate index file against signature
  !insertmacro verifySignature "${PATCH_INDEX}"

  ; If error, clean up vars and skip update
  IfErrors +1 IndexDownloadSuccess
    Goto UpdateCheckError
  IndexDownloadSuccess:

  ; Get GE:S minor version, this determines which update section to read
  Push ${PROD_VERSION}
  Call GetMinorVersion
  Pop $0

  ; we'll check the minimum installer_version real quick before anything else,
  ; to see if the user should be prompted to update

  ${ReadVDFStr} $1 "${PATCH_INDEX}" "update>installer_version"
  ${VersionCompare} "$1" "${INSTALLER_BUILD_TIME}" $1

  ; $2 = URL to send user to for update
  ${ReadVDFStr} $2 "${PATCH_INDEX}" "update>download_page"

  ; If version 1 (aka minimum version) is greater (than installer version),
  ; prompt user to update.
  StrCmp $1 "1" +1 UpdateCheckContinue

    ; FULL Installer -- if new installer available, let them elect whether to
    ; download it or not. If they choose not to, the installer will continue,
    ; but they will not be able to install updates from the internet.
    !ifdef FULL
      MessageBox MB_YESNO "$(NEW_INSTALLER_AVAILABLE_FULL)" IDNO InstallerUpdateDeclined
        ExecShell open "$2"
        Quit

        InstallerUpdateDeclined:
          MessageBox MB_OK "$(NEW_INSTALLER_DECLINED_FULL)"
          Goto UpdateCheckEnd

    ; NETWORK Installer -- if new installer available, alert user, open webpage,
    ; and then quit.
    !else
      MessageBox MB_OK "$(NEW_INSTALLER_AVAILABLE_NETINSTALL)"
      ExecShell open "$2"
      Quit
    !endif

  ; proceed processing update index file
  UpdateCheckContinue:

  ${ReadVDFStr} $GES_UPDATE_VERSION "${PATCH_INDEX}" "update>version"
  ${ReadVDFStr} $GES_UPDATE_CHANGELOG "${PATCH_INDEX}" "update>changelog"
  ${ReadVDFStr} $GES_TORRENT "${PATCH_INDEX}" "update>torrent"
  !ifndef FULL
    ${ReadVDFStr} $GES_INSTALL_SIZE "${PATCH_INDEX}" "update>install_size"
    ${ReadVDFStr} $GES_DOWNLOAD_SIZE "${PATCH_INDEX}" "update>download_size"
  !endif

  ; Validate download and install sizes, default to 0 if validation doesn't work
  ${Validate} $1 $GES_INSTALL_SIZE "${NUMERIC}"
  StrCmp $1 1 InstallSizeNumeric
    StrCpy $GES_INSTALL_SIZE 0
  InstallSizeNumeric:

  ${Validate} $1 $GES_DOWNLOAD_SIZE "${NUMERIC}"
  StrCmp $1 1 DownloadSizeNumeric
    StrCpy $GES_DOWNLOAD_SIZE 0
  DownloadSizeNumeric:

  ; Set the update URL based on the currently installed version of GE:S.
  ; Use patch_previous if the version of GE:S defined by version_previous is installed
  ; Use patch_any otherwise
  ${ReadVDFStr} $1 "${PATCH_INDEX}" "update>version_previous"
  ${VersionCompare} $GES_INSTALLED_VERSION $1 $1
  StrCmp $1 0 PreviousVersionEqual PreviousVersionNotEqual
    PreviousVersionEqual:
      ${ReadVDFStr} $GES_UPDATE_URL "${PATCH_INDEX}" "update>patch_previous"
      Goto PreviousVersionDone

    PreviousVersionNotEqual:
      ${ReadVDFStr} $GES_UPDATE_URL "${PATCH_INDEX}" "update>patch_any"

  PreviousVersionDone:

  Goto UpdateCheckEnd
  UpdateCheckError:
    !ifdef PATCH
      MessageBox MB_OK|MB_ICONSTOP $(UPDATE_CHECK_FAILED_CRITICAL)
      Quit
      Goto UpdateCheckBegin ; workaround compiler warning
    !else
      ; Promp user to retry
      MessageBox MB_YESNO "$(UPDATE_CHECK_FAILED)" IDYES UpdateCheckBegin
      ; If user said no to "retry?" prompt, display final dialog and reset vars
      MessageBox MB_OK "$(UPDATE_CHECK_DECLINED)"
      Call clearUpdateInfo
    !endif

  UpdateCheckEnd:
  Delete "${PATCH_INDEX}"
  !undef PATCH_INDEX

  ; If update version is not set or less than installer version,
  ; set it to installer version
  ${VersionCompare} "$GES_UPDATE_VERSION" "${PROD_VERSION}" $0
  StrCmp $0 0 SetToSame +1
  StrCmp $0 2 SetToSame +1
  StrCmp $GES_UPDATE_VERSION "" SetToSame DontSetToSame
  SetToSame:
  StrCpy $GES_UPDATE_VERSION "${PROD_VERSION}"
  DontSetToSame:

  ; Update check end

  ; Is GE:S updateable?
  ; an install is updateable if the installed version and new version are of the
  ; same minor version (x.x).
  Push $GES_INSTALLED_VERSION
  Call GetMinorVersion
  Pop $0
  Push ${PROD_VERSION}
  Call GetMinorVersion
  Pop $1

  StrCpy $GES_UPDATEABLE 0 ; default value, will get overridden if necessary

  ; if minor versions are not the same, not updateable
  ${VersionCompare} "$0" "$1" $0
  StrCmp $0 0 +1 not_updateable
  ; if installer version equal or less than installed version, not updateable
  ${VersionCompare} "$GES_INSTALLED_VERSION" "$GES_UPDATE_VERSION" $0
  StrCmp $0 2 +1 not_updateable
    ; we passed the checks up to this point, mark as updateable
    StrCpy $GES_UPDATEABLE 1

  not_updateable:
  ; Updateable end

  ; The full installer will use the built-in data size only
  !ifdef FULL
    StrCpy $GES_INSTALL_SIZE "${INSTALL_SIZE}"
    StrCpy $GES_DOWNLOAD_SIZE 0
  !endif

  ; Initial value, this may change based on install options
  StrCpy $DISK_REQUIRED $GES_INSTALL_SIZE

  !ifdef PATCH
    Call autoUpdate_PreInstallChecks
  !endif

  StrCpy $GES_UPDATE_CHECK_COMPLETED 1

  ; This prevents the Update page from being shown
  Abort
FunctionEnd

; Install Options page
; From nsDialogs readme:
; Pages in Modern UI are 300 dialogs units wide and 140 dialog units high

; This install options page, and the relevant functions, are not used in the patch installer.
!ifndef PATCH
Function InstallOptsPage
  !insertmacro MUI_HEADER_TEXT $(INSTALLOPTS_H1) $(INSTALLOPTS_H2)
  nsDialogs::Create 1018

    ; Install directory/free space groupbox
    ${NSD_CreateGroupBox} 0u 0u 300u 56u $(INST_TO)

      ${NSD_CreateText} 6u 12u 230u 12u "$INSTDIR"
      Pop $txtInstDir
      SendMessage $txtInstDir ${EM_SETREADONLY} 1 0

      ${NSD_CreateButton} 244u 11u 50u 15u $(INST_BROWSE)
      Pop $btnInstDir
      ${NSD_OnClick} $btnInstDir "browseInstDir"

      ; Label only created if GE:S is currently installed
      StrCmp "$GES_INSTALLED_VERSION" "" Skip_1 +1
        ${NSD_CreateLabel} 6u 30u 182u 24u $(INST_DIR_CHANGE_NOT_PERMITTED)
        Pop $0

        EnableWindow $btnInstDir 0
      Skip_1:

      ; Link to reset instdir to default. Always created but is shown/hidden
      ${NSD_CreateLink} 6u 30u 92u 12u $(INST_DIR_RESET_TO_DEFAULT)
      Pop $lnkResetInstDir
      ShowWindow $lnkResetInstDir 0 ; Hide until needed
      ${NSD_OnClick} $lnkResetInstDir "resetInstDir" ; Execute function on click

      ${NSD_CreateLabel} 194u 30u 40u 10u $(DISK_REQ)
      Pop $0

      ${NSD_CreateLabel} 194u 40u 40u 10u $(DISK_FREE)
      Pop $0

      ${NSD_CreateLabel} 254u 30u 40u 10u ""
      Pop $lblDiskRequiredAmount
      ${NSD_AddStyle} $lblDiskRequiredAmount ${SS_RIGHT}

      ${NSD_CreateLabel} 254u 40u 40u 10u ""
      Pop $lblDiskFreeAmount
      ${NSD_AddStyle} $lblDiskFreeAmount ${SS_RIGHT}

    ; Groupbox end

    ; Update options
    StrCmp "$GES_INSTALLED_VERSION" "" Skip_2 +1
      ${NSD_CreateGroupBox} 0u 64u 300u 52u $(UPDATE_OPTIONS)
      Pop $0

        ${NSD_CreateRadioButton} 6u 74u 276u 12u $(INSTALL_UPDATE)
        Pop $radUpdate
        ${NSD_SetState} $radUpdate ${BST_CHECKED} ; mark by default
        ${NSD_OnClick} $radUpdate "switchInstallModeCallback"

        ${NSD_CreateRadioButton} 6u 86u 264u 12u $(REINSTALL)
        Pop $radReinstall
        ${NSD_OnClick} $radReinstall "switchInstallModeCallback"

        ${NSD_CreateCheckBox} 18u 98u 200u 12u $(KEEP_ACHIEVEMENTS)
        Pop $chkKeepAchievements
        ${NSD_SetState} $chkKeepAchievements ${BST_CHECKED} ; mark by default
      ; Groupbox end
    Skip_2:

    ; Start shotcut checkbox
    ${NSD_CreateCheckBox} 0u 124u 148u 10u "$(CREATE_START_ICON)"
    Pop $chkCreateShortcut

    !ifdef NETWORK
      ; Seed torrent checkbox
      ${NSD_CreateCheckBox} 152u 124u 142u 10u "$(SEED_TORRENT)"
      Pop $chkSeedTorrent

      ; Seed torrent help link
      ${NSD_CreateLink} 294u 124u 6u 10u " ? "
      Pop $lnkSeedTorrent
      ${NSD_OnClick} $lnkSeedTorrent "mbSeedTorrent"
    !endif

    ; Function calls to modify the page based on user environment
    Call updateDiskFreeLabel ; Initial update of disk free label
    Call updateDiskRequired ; initial update of label + var $DISK_REQUIRED
    Call disableUpdateOptionIfNotUpdateable
    Call switchInstallModeCallback

    ; Lastly, restore control states (e.g. if the user hit the back button, then next)
    ; Save states if this is the first run -- ensure we don't restore empty states
    StrCmp $InstallOptionsFirstRun "" +1 SkipSave
      StrCpy $InstallOptionsFirstRun 0
      Call chkCreateShortcut_FirstRun
      Call SaveInstallOptionStates
      !ifdef NETWORK
        StrCpy $chkSeedTorrent_Checked 1
      !endif
    SkipSave:
    ; Restore states
    Call RestoreInstallOptionStates
    ; Save states when clicking back
    ${NSD_OnBack} "SaveInstallOptionStates"

  nsDialogs::Show
FunctionEnd

Function SaveInstallOptionStates
  ${NSD_GetState} $radUpdate $radUpdate_Checked
  ${NSD_GetState} $radReinstall $radReinstall_Checked
  ${NSD_GetState} $chkKeepAchievements $chkKeepAchievements_Checked
  ${NSD_GetState} $chkCreateShortcut $chkCreateShortcut_Checked
  !ifdef NETWORK
    ${NSD_GetState} $chkSeedTorrent $chkSeedTorrent_Checked
  !endif
FunctionEnd

Function RestoreInstallOptionStates
  ${NSD_SetState} $radUpdate $radUpdate_Checked
  ${NSD_SetState} $radReinstall $radReinstall_Checked
  ${NSD_SetState} $chkKeepAchievements $chkKeepAchievements_Checked
  ${NSD_SetState} $chkCreateShortcut $chkCreateShortcut_Checked
  !ifdef NETWORK
    ${NSD_SetState} $chkSeedTorrent $chkSeedTorrent_Checked
  !endif
  Call switchInstallModeCallback
  Call updateDiskRequired
FunctionEnd

Function chkCreateShortcut_FirstRun
  ; run the first time the install options menu is displayed
  ; check start menu box if GE:S not installed, or if icon exists
  ; Otherwise (GE:S is installed but the start menu icon doesn't exist)
  ; don't check this box

  ; This is a way of making sure if the user doesn't want the Start icon,
  ; the checkbox stays unchecked by default

  ; if GE:S not installed, set box as checked by default
  StrCmp $GES_INSTALLED_VERSION "" +1 Installed
    ${NSD_SetState} $chkCreateShortcut "${BST_CHECKED}"
    Return
  Installed:

  ; if icon exists, set box as checked by default
  SetShellVarContext all
  IfFileExists "$SMPROGRAMS\${FILE_NAME}.url" +1 StartIconDoesntExist
    ${NSD_SetState} $chkCreateShortcut "${BST_CHECKED}"
    Return
  StartIconDoesntExist:

  ; Set box unchecked by default
  ${NSD_SetState} $chkCreateShortcut "${BST_UNCHECKED}"
  Return
FunctionEnd

Function updateDiskRequired
  Push $R0
  Push $R1

  StrCmp $GES_UPDATEABLE 1 Updateable +1
    !ifdef FULL
      StrCpy $DISK_REQUIRED $GES_INSTALL_SIZE
    !else ; network installer

      ; Get volume GUIDs so we can check if INSTDIR and PLUGINSDIR are on the
      ; same drive

      ; Get $INSTDIR GUID, if not already set
      ; AND a different path from last time

      StrCmp $UDR_InstdirPath $INSTDIR InstdirIsSame +1

      Push $INSTDIR
      Call getVolumeGUID
      Pop $R1
      StrCpy $UDR_InstdirPath $INSTDIR
      StrCpy $UDR_InstdirGUID $R1

      InstdirIsSame:
      StrCpy $R1 $UDR_InstdirGUID

      ; If pluginsdir + instdir are on the same drive,
      ; include download size in space required
      StrCmp $R0 $R1 +1 OnDifferentDrives
        IntOp $DISK_REQUIRED $GES_INSTALL_SIZE + $GES_DOWNLOAD_SIZE
        Goto OnSameDrive

      ; if pluginsdir + instdir are on different drives,
      ; require only the install size.
      ; pluginsdir space required is calculated separately on page leave.
      OnDifferentDrives:
        StrCpy $DISK_REQUIRED $GES_INSTALL_SIZE

      OnSameDrive:

    !endif
    Goto end
  Updateable:
  ${NSD_GetState} $radReinstall $R0
  StrCmp $R0 ${BST_CHECKED} reinstall update
  reinstall:
    IntOp $DISK_REQUIRED $GES_INSTALL_SIZE + $GES_DOWNLOAD_SIZE
    Goto end
  update:
    ; these updates are typically very small, so we won't worry about storage
    ; requirements
    StrCpy $DISK_REQUIRED 0
    Goto end
  end:

  ; update label; this section should always run

  StrCmp $DISK_REQUIRED 0 NoDiskReq DiskReq
  DiskReq:
    ${NSD_SetText} $lblDiskRequiredAmount "$DISK_REQUIRED$(MEGABYTE)"
    Goto DiskReqEnd
  NoDiskReq:
    ${NSD_SetText} $lblDiskRequiredAmount "$(NA)"
    Goto DiskReqEnd
  DiskReqEnd:

  Pop $R1
  Pop $R0
FunctionEnd

Function disableUpdateOptionIfNotUpdateable
  StrCmp $GES_UPDATEABLE 1 +1 Disable
  Return

  Disable:
  ${NSD_SetState} $radUpdate ${BST_UNCHECKED}
  ${NSD_SetText} $radUpdate $(UPDATE_NOT_POSSIBLE)
  ${NSD_SetState} $radReinstall ${BST_CHECKED}
  EnableWindow $radUpdate 0
  Call switchInstallModeCallback
  Call updateDiskRequired
FunctionEnd

Function switchInstallModeCallback
  Call updateDiskRequired

  ; Enable/Disable the "Keep Achievements" checkbox
  ${NSD_GetState} $radUpdate $0
  StrCmp $0 ${BST_CHECKED} update_checked reinstall_checked

  update_checked:
  ; Even though these checkboxes don't apply to "update",
  ; we want to set an appropriate state to avoid confusing users
  ${NSD_SetState} $chkKeepAchievements ${BST_CHECKED}
  EnableWindow $chkKeepAchievements 0

  !ifdef NETWORK
    ${NSD_SetState} $chkSeedTorrent ${BST_UNCHECKED}
    EnableWindow $chkSeedTorrent 0
    EnableWindow $lnkSeedTorrent 0
  !endif

  Return

  reinstall_checked:
  EnableWindow $chkKeepAchievements 1

  !ifdef NETWORK
    ${NSD_SetState} $chkSeedTorrent ${BST_CHECKED}
    EnableWindow $chkSeedTorrent 1
    EnableWindow $lnkSeedTorrent 1
  !endif
  Return
FunctionEnd

Function updateDiskFreeLabel
  Push $0

  Push $INSTDIR
  Call getDiskFree
  Pop $0

  ${NSD_SetText} $lblDiskFreeAmount "$0$(MEGABYTE)"

  ; If larger than 999999MB, truncate to "999999+MB"
  IntCmp $0 999999 +1 Skip_TruncateNum +1
    ${NSD_SetText} $lblDiskFreeAmount "999999+$(MEGABYTE)"
  Skip_TruncateNum:

  Pop $0
FunctionEnd

Function browseInstDir
  Call changeInstDir

  StrCmp $SOURCEMODS\${MODDIR} $INSTDIR Done +1 ; Avoid showing "reset" link if setting to the default dir
    ${NSD_SetText} $txtInstDir $INSTDIR ; Update textbox
    ShowWindow $lnkResetInstDir 1 ; Show reset link
    Call updateDiskRequired
    Call updateDiskFreeLabel

  Done:
FunctionEnd

; On clicking the reset link, display a confirmation dialog
; if user clicks yes, reset instdir and hide the no-longer-needed reset link
Function resetInstDir
  MessageBox MB_YESNO "$(INST_DIR_CONFIRM_RESET)$\r$\n$SOURCEMODS\${MODDIR}" IDNO NO
    StrCpy $INSTDIR "$SOURCEMODS\${MODDIR}"
    ${NSD_SetText} $txtInstDir $INSTDIR
    Call updateDiskRequired
    Call updateDiskFreeLabel
    ShowWindow $lnkResetInstDir 0
  NO:
FunctionEnd

!ifdef NETWORK
  Function mbSeedTorrent
    MessageBox MB_OK "$(SEED_TORRENT_MB)"
  FunctionEnd
!endif

Function InstallOptsLeave
  ; NETWORK INSTALLER ONLY --
  ; Check that enough space exists at $PLUGINSDIR
  !ifndef FULL

    Push $PLUGINSDIR
    Call getDiskFree
    Pop $0

    IntCmp $GES_DOWNLOAD_SIZE $0 +1 SkipTempDiskSpace +1
      MessageBox MB_OK "$(DISK_INSUFFICIENT_TEMP_SPACE)"
      Abort
    SkipTempDiskSpace:

  !endif

  ; Validate that enough space is free at $INSTDIR
  Push $INSTDIR
  Call getDiskFree
  Pop $0
  IntCmp $DISK_REQUIRED $0 +1 SkipDiskSpace
    StrCmp $GES_INSTALLED_VERSION "" MessageBox2 MessageBox1
      ; If GE:S is installed, optionally allow the user to disregard disk requirements
      ; If GE:S is not installed, enforce the disk requirement and don't let them install
      MessageBox1:
        MessageBox MB_YESNO "$(DISK_INSUFFICIENT_SPACE)" IDYES SkipDiskSpace
        Abort
      MessageBox2:
        MessageBox MB_OK "$(DISK_INSUFFICIENT_SPACE2)"
        Abort
  SkipDiskSpace:

  ; Save control states
  Call SaveInstallOptionStates

  ; Set sections based on control states

  ; Start menu shortcut
  ${NSD_GetState} $chkCreateShortcut $0
  StrCmp "$0" "${BST_CHECKED}" Checked +1
    SectionSetFlags ${CREATE_SHORTCUT} 0
  Checked:

  ; Default value; overridden as necessary
  StrCpy $INSTALL_OR_UPDATE 'INSTALL'

  ; if network installer, the torrent should always be the latest so we disable
  ; the patch download unless re-enabled later
  !ifdef NETWORK
    SectionSetFlags ${PATCH_DOWNLOAD} 0
    SectionSetFlags ${PATCH_INSTALL} 0
  !endif

  ; If GE:S is installed (and therefore the update groupbox is displayed)
  StrCmp $GES_INSTALLED_VERSION "" not_updateable +1
    ${NSD_GetState} $radReinstall $0
    ${NSD_GetState} $chkKeepAchievements $1
    StrCmp $0 ${BST_CHECKED} reinstall update
    reinstall:
      ; if keep achievements checked, goto end_section
      StrCmp $1 ${BST_CHECKED} end_section_set +1
        ; unmark the achievements section
        SectionSetFlags ${BACKUP_ACHIEVEMENTS} 0
      Goto end_section_set
    update:
      StrCpy $INSTALL_OR_UPDATE 'UPDATE'
      SectionSetFlags ${UNINSTALL_OLD_VER} 0
      SectionSetFlags ${CREATE_INSTDIR} 0

      !ifdef FULL
        ; Don't validate local archive for an update-only operation.
        SectionSetFlags ${VALIDATE_LOCAL_ARCHIVE} 0
      !endif

      !ifdef NETWORK
        SectionSetFlags ${GES_DOWNLOAD} 0

        ; Re-enable update download/install for betwork installer
        SectionSetFlags ${PATCH_DOWNLOAD} 1
        SectionSetFlags ${PATCH_INSTALL} 1
      !endif

      !ifndef PATCH
        ; This isn't compiled into the patch installer
        SectionSetFlags ${SET_DISPLAY_RESOLUTION} 0
      !endif
      
      SectionSetFlags ${GES_INSTALL} 0
      ; Data will not be removed; no need to back up or restore it
      SectionSetFlags ${BACKUP_ACHIEVEMENTS} 0
      SectionSetFlags ${BACKUP_CONFIG} 0
      SectionSetFlags ${RESTORE_DATA} 0
      Goto end_section_set
  not_updateable:
    ; if GE:S not installed
    Goto end_section_set
  end_section_set:
FunctionEnd
!endif