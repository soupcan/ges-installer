; Signature validation function/macro using Minisign.
; Sets the errors flag if validation fails

; Requires ${PUBLIC_KEY} to be defined and set to the Minisign public key.
; If $SIGNATURE_CHECKING_ENABLED is 0, it will skip verification and the errors
; flag will NOT be set no matter what.

; Usage: !insertmacro verifySignature "file"
; "file" is the file that will be checked against the signature.
; Minisign automatically assumes that the signature will be stored in
; "file.minisig" in the same directory.

Function verifySignatureFunction
	IfFileExists "$PLUGINSDIR\minisign.exe" SignatureCheck
		Push $OUTDIR
		StrCpy $OUTDIR $PLUGINSDIR
		File "bin\minisign.exe"
		Pop $OUTDIR

	SignatureCheck:
		Exch $0 ; Swap $0 with the top value on the stack
		
		DetailPrint "Verifying signature $0"
		nsExec::ExecToLog '"$PLUGINSDIR\minisign.exe" -Vm "$0" -P "${PUBLIC_KEY}"'
	  	Pop $0 ; Get the Minisign return value off the stack

	  	DetailPrint "Minisign returned $0"

	  	StrCmp $0 0 +3 +1
	  		DetailPrint "verifySignature: Setting error flag."
	  		SetErrors

	  	; Restore $0 to its original value
	  	Exch $0
FunctionEnd

!macro verifySignature file
	!define SigCheckDisabled "Label_${__LINE__}"
	!define SigCheck "Label_${__LINE__}"

	StrCmp $SIGNATURE_CHECKING_ENABLED 0 +1 ${SigCheck}
		DetailPrint "verifySignature: Signature checking disabled on the command line. SKIPPED."
		Goto ${SigCheckDisabled}

	${SigCheck}:
	Push "${file}"
	Call verifySignatureFunction
		
	${SigCheckDisabled}:
	!undef SigCheckDisabled
	!undef SigCheck
!macroend