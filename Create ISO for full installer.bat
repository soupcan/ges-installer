@ECHO OFF

WHERE OSCDIMG.EXE > NUL
IF %ERRORLEVEL% NEQ 0 (
	ECHO.Could not locate OSCDIMG.EXE.
	ECHO.
	ECHO.1. Download and install the Deployment Tools of the Windows ADK
	ECHO.2. Add the location of OSCDIMG.EXE to your ^%PATH^%
	ECHO.3. Run this script again
	PAUSE
	EXIT /B
)

IF [%1] == [] (
	ECHO.Command-line parameter not set. Either pass the source path to this script,
	ECHO.OR ^(recommended^) drag the source folder on top of this batch script.
	PAUSE
	EXIT /B
)

ECHO.What version number of GE:S are you releasing? Include the whole version string.
ECHO.This will appear in the file name as well as the volume label.
ECHO.
SET /P VER=Enter version string (ex. v5.0.6): v

@ECHO ON

OSCDIMG.EXE -u2 -w4 -m "%1" "%~dp0\GoldenEye Source v%VER%.iso" -l"GoldenEye: Source v%VER%"

@ECHO OFF
PAUSE